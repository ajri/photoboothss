@echo off
REM Masukkan jalur lengkap ke file AHK di bawah ini
set AHK_SCRIPT=C:\Users\HP\Documents\NetBeansProjects\mavenproject1\target\minimize_window2.ahk

REM Periksa apakah file AHK ada
if not exist "%AHK_SCRIPT%" (
    echo File AHK tidak ditemukan: %AHK_SCRIPT%
    exit /b 1
)

REM Jalankan skrip AHK
echo Menjalankan skrip AHK: %AHK_SCRIPT%
start "" /min /wait AutoHotkey.exe "%AHK_SCRIPT%"

REM Periksa kode keluaran
if %errorlevel% equ 0 (
    echo Skrip AHK berhasil dijalankan.
    exit /b 0
) else (
    echo Terjadi kesalahan saat menjalankan skrip AHK.
    exit /b 1
)

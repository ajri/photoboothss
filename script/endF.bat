@echo off

REM Inisialisasi variabel untuk melacak apakah A.exe sudah dibuka
set "app_opened=false"

REM Menangkap argumen dari dslrBooth
set event_type=%1
set param1=%2
set param2=%3

REM Mengecek jika event_type adalah session_end
if "%event_type%"=="session_end" (
    REM Mengecek apakah A.exe belum dibuka sebelumnya
    if "%app_opened%"=="false" (
        echo Session has ended. Opening another application...
        
        REM Ganti "C:\Users\10\Documents\NetBeansProjects\mavenproject1\target\A.exe" dengan path lengkap ke aplikasi yang ingin Anda buka

        REM Menandai bahwa A.exe sudah dibuka
        set "app_opened=true"

        REM Menjalankan skrip AutoHotkey untuk mengontrol jendela A.exe
        start "" "C:\Users\HP\Documents\NetBeansProjects\mavenproject1\target\minimize_window.ahk"
    )
)

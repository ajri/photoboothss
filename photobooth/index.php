<!DOCTYPE html>
<html>

<head>
  <meta charset="utf-8" />
  <meta http-equiv="X-UA-Compatible" content="IE=edge" />
  <title>Card</title>
  <meta name="viewport" content="width=device-width, initial-scale=1.0" />
  <link rel="stylesheet" href="index.css">

  <script type="text/javascript"
    src="https://app.sandbox.midtrans.com/snap/snap.js"
    data-client-key="SB-Mid-client-EjxIhSXOMk_1BSNJ"></script>

  <!-- Sambungkan file JavaScript terpisah -->
</head>
<body>
<div class="container">
    <img src="src/Group1.png">
    <div class="overlay"></div>

    <div class="d"></div>
    <div class="e"></div>

    <div class="main">
      <img src="src/Logo.png">
      <div class="sc">
        <div class="sd">
        <img class="g2" src="src/Group2.png">
        <img class="g3"  src="src/Group3.png">
        <img class="g4"  src="src/Group4.png">
        <img class="g5" src="src/Group5.png">
          <button type="button" class="btn" id="startBtn">Start Here</button>
        </div>
      </div>
    </div>
  </div>
  <script src="my.js"></script>
  <script src="cb.js"></script>


</body>

</html>
